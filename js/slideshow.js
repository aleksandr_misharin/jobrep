(function() {
  'use strict';

  var slideshow = [{
    "image": "images/gallery_1.jpeg",
    "caption": "Cloudy with a chance of moon"
  }, {
    "image": "images/gallery_2.jpeg",
    "caption": "Half moon mountain"
  }, {
    "image": "images/gallery_3.jpeg",
    "caption": "Moonrise"
  }];
  
  var divslideshow  = $('div.gallery-wrapper'), caption = $('#caption');
var show_img = ' class="current" ', current_img = '', default_img = '';
var slide_first=0,slide_last = slideshow.length-1, slide_current = slide_first;
	for (var i=0; i<=slide_last; i++){
	//1ый слайды
	if (i == slide_first)  {
		current_img = show_img;
		caption.text(slideshow[i]['caption']);
		//кнопки вначале и конце	
		if (slide_current==slide_first)  $('#prev').attr('disabled','disabled'); else $('#prev').removeAttr('disabled');
		if (slide_current==slide_last)   $('#next').attr('disabled','disabled'); else $('#next').removeAttr('disabled');
	}
	else{ 
		current_img = default_img;
	}
	divslideshow.append('<img src="'  +slideshow[i]['image']  +'" '+'alt="'  +slideshow[i]['caption']+'" '+'title="'+slideshow[i]['caption']+'" '+current_img+' />');
  } 

$('#prev').click(function()
	{if (slide_current > slide_first) {
		slide_current = slide_current-1;
		$('#next').removeAttr('disabled');
		var slide_current_img = $('.current').prev('img');
		slide_current_img.addClass('current').hide().fadeIn(0.5).next('img').removeClass('current').hide();
		caption.text(slide_current_img.attr('alt'));} 
	if (slide_current == slide_first) {$(this).attr('disabled','disabled');}
	});
	
$('#next').click(function()
	{if (slide_current < slide_last) {
		slide_current = slide_current+1;
		$('#prev').removeAttr('disabled');
		var slide_current_img = $('.current').next('img');
		slide_current_img.addClass('current').hide().fadeIn(0.5).prev('img').removeClass('current').hide();
		caption.text(slide_current_img.attr('alt'));}
	if (slide_current == slide_last) {$(this).attr('disabled','disabled');}
	});
})();
